/*
 * lcdauto.h
 *
 *  Created on: Feb 15, 2016
 *      Author: Benson
 */

#ifndef INCLUDE_LCDAUTO_H_
#define INCLUDE_LCDAUTO_H_

// Allow usage of this file in C++ programs
#ifdef __cplusplus
extern "C" {
#endif

extern int auton;
void lcdAuto();
void lcdSensor();

// End C++ export structure
#ifdef __cplusplus
}
#endif

#endif /* INCLUDE_LCDAUTO_H_ */
