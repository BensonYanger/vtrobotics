/*
 * drive.h
 *
 *  Created on: Jan 26, 2016
 *      Author: Benson
 */

#ifndef DRIVE_H_
#define DRIVE_H_


// Allow usage of this file in C++ programs
#ifdef __cplusplus
extern "C" {
#endif

void driveTask();

// End C++ export structure
#ifdef __cplusplus
}
#endif

#endif /* DRIVE_H_ */
