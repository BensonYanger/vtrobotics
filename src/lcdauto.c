/*
 * lcdauto.c
 *
 *  Created on: Feb 15, 2016
 *      Author: Benson
 */

#include "main.h"
#include "lcdauto.h"

void lcdAuto() {
		//Leave this value alone
		int lcdScreenMin = 1;
		//This keeps track of which program you want to run
		int lcdScreen = 1;
		//Change this value to be the maximum number of programs you want on the robot
		int lcdScreenMax = 5;

		char lcdAStr[32];

		while (true) {
			if (lcdReadButtons(uart1 ) == 1) { //Scrolls to the left
				if (lcdScreenMin == lcdScreen) {
					lcdScreen = lcdScreenMax;
					delay(250);
				} else {
					lcdScreen --;
					delay(250);
				}
			}
			if (lcdReadButtons(uart1 ) == 4) { //Scrolls to the right
				if (lcdScreenMax == lcdScreen) {
					lcdScreen = lcdScreenMin;
					delay(250);
				} else {
					lcdScreen++;
					delay(250);
				}
			}
			if (lcdScreen == 1 && auton != 1) {
				sprintf(lcdAStr, "       1        "); //Name the first program here
				lcdSetText(uart1, 1, lcdAStr);
				sprintf(lcdAStr, "   Full Court   "); //Name the first program here
				lcdSetText(uart1, 2, lcdAStr);
				if (lcdReadButtons(uart1 ) == 2) {
					auton = lcdScreen; //Sets the Program to the one on-screen
					sprintf(lcdAStr, "Autonomous Has");
					lcdSetText(uart1, 1, lcdAStr);
					sprintf(lcdAStr, "Been Selected!");
					lcdSetText(uart1, 2, lcdAStr);
					delay(1000);
				}
			} else if (lcdScreen == 1 && auton == 1) {
				sprintf(lcdAStr, "      [1]       "); //We use brackets to mark which program we have chosen
				lcdSetText(uart1, 1, lcdAStr);
				sprintf(lcdAStr, "   Half Court   "); //So that while we're scrolling, we can have one marked
				lcdSetText(uart1, 2, lcdAStr);
			} else if (lcdScreen == 2 && auton != 2) {
				sprintf(lcdAStr, "       2        "); //Name the second program here
				lcdSetText(uart1, 1, lcdAStr);
				sprintf(lcdAStr, "    Bar Shot    "); //Name the second program here
				lcdSetText(uart1, 2, lcdAStr);
				if (lcdReadButtons(uart1 ) == 2) {
					auton = lcdScreen; //Sets the Program to the one on-screen
					sprintf(lcdAStr, "Autonomous Has");
					lcdSetText(uart1, 1, lcdAStr);
					sprintf(lcdAStr, "Been Selected!");
					lcdSetText(uart1, 2, lcdAStr);
					delay(1000);
				}
			} else if (lcdScreen == 2 && auton == 2) {
				sprintf(lcdAStr, "      [2]       "); //We use brackets to mark which program we have chosen
				lcdSetText(uart1, 1, lcdAStr);
				sprintf(lcdAStr, "    Bar Shot    "); //So that while we're scrolling, we can have one marked
				lcdSetText(uart1, 2, lcdAStr);
			} else if (lcdScreen == 3 && auton != 3) {
				sprintf(lcdAStr, "       3        "); //Name the third program here
				lcdSetText(uart1, 1, lcdAStr);
				sprintf(lcdAStr, " Delayed Bar 3s "); //Name the third program here
				lcdSetText(uart1, 2, lcdAStr);
				if (lcdReadButtons(uart1 ) == 2) {
					auton = lcdScreen; //Sets the Program to the one on-screen
					sprintf(lcdAStr, "Autonomous Has");
					lcdSetText(uart1, 1, lcdAStr);
					sprintf(lcdAStr, "Been Selected!");
					lcdSetText(uart1, 2, lcdAStr);
					delay(1000);
				}
			} else if (lcdScreen == 3 && auton == 3) {
				sprintf(lcdAStr, "      [3]       "); //We use brackets to mark which program we have chosen
				lcdSetText(uart1, 1, lcdAStr);
				sprintf(lcdAStr, " Delayed Bar 3.5s "); //So that while we're scrolling, we can have one marked
				lcdSetText(uart1, 2, lcdAStr);
			} else if (lcdScreen == 4 && auton != 4) {
				sprintf(lcdAStr, "       4        "); //Name the fourth program here
				lcdSetText(uart1, 1, lcdAStr);
				sprintf(lcdAStr, " Delayed Bar 7s "); //Name the fourth program here
				lcdSetText(uart1, 2, lcdAStr);
				if (lcdReadButtons(uart1 ) == 2) {
					auton = lcdScreen; //Sets the Program to the one on-screen
					sprintf(lcdAStr, "Autonomous Has");
					lcdSetText(uart1, 1, lcdAStr);
					sprintf(lcdAStr, "Been Selected!");
					lcdSetText(uart1, 2, lcdAStr);
					delay(1000);
				}
			} else if (lcdScreen == 4 && auton == 4) {
					sprintf(lcdAStr, "      [4]       "); //We use brackets to mark which program we have chosen
					lcdSetText(uart1, 1, lcdAStr);
					sprintf(lcdAStr, " Delayed Bar 7s "); //So that while we're scrolling, we can have one marked
					lcdSetText(uart1, 2, lcdAStr);
			} else if (lcdScreen == 5 && auton != 5) {
					sprintf(lcdAStr, "       5        "); //Name the third program here
					lcdSetText(uart1, 1, lcdAStr);
					sprintf(lcdAStr, " Program Skills "); //Name the third program here
					lcdSetText(uart1, 2, lcdAStr);
					if (lcdReadButtons(uart1 ) == 2) {
						auton = lcdScreen; //Sets the Program to the one on-screen
						sprintf(lcdAStr, "Autonomous Has");
						lcdSetText(uart1, 1, lcdAStr);
						sprintf(lcdAStr, "Been Selected!");
						lcdSetText(uart1, 2, lcdAStr);
						delay(1000);
					}
			} else if (lcdScreen == 5 && auton == 5) {
				sprintf(lcdAStr, "      [5]       "); //We use brackets to mark which program we have chosen
				lcdSetText(uart1, 1, lcdAStr);
				sprintf(lcdAStr, " Program Skills "); //So that while we're scrolling, we can have one marked
				lcdSetText(uart1, 2, lcdAStr);
			}
			delay(100);
		}
}

/*
void lcdSensor() {
	int lcdStr[32];
	while(1) {
		sprintf(lcdStr, "%5d %5d", encoderGet(driveL), encoderGet(flywheel));
		lcdSetText(uart2, 1, lcdStr);
		sprintf(lcdStr, "%5d %5d", analogRead(4), analogRead(3));
		lcdSetText(uart2, 2, lcdStr);
		delay(15);
	}
}
*/
